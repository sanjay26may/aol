package helper

import (
	"reflect"
)

func String(i interface{}) string {
	if i == nil {
		return ""
	}
	t := reflect.TypeOf(i)
	if t.Kind() == reflect.String {
		return i.(string)
	}
	return ""
}

//test
