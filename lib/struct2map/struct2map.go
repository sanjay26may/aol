// Package s2m is to convert structure having json tag to map
package s2m

import (
	"reflect"
	"strconv"
	"strings"
)

// StructToMap takes struct s as argument.
// input structure must have json tag with field name.
// json tag name will be used as field name in map.
// returns map[string]interface{}
func StructToMap(i interface{}) map[string]interface{} {
	ch := make(map[string]interface{})
	structToMap(i, "", 0, ch)
	return ch
}

func structToMap(i interface{}, p string, indexCounter int, ch map[string]interface{}) {
	k := reflect.TypeOf(i)
	for l := 0; l < k.NumField(); l++ {
		f := k.Field(l)
		tag := f.Tag.Get("json")
		tag = strings.Split(tag, ",")[0]

		fp := ""

		if indexCounter != 0 {
			fp = p + "[" + strconv.Itoa(indexCounter-1) + "]" + "." + tag
		} else {
			if p == "" {
				fp = tag
			} else {
				fp = p + "." + tag
			}

		}

		v := reflect.ValueOf(i).Field(l)
		if v.Kind() == reflect.Struct {

			structToMap(v.Interface(), fp, 0, ch)

		} else if v.Kind() == reflect.Slice {

			ll := v.Len()
			ch[fp+"[*]"] = ll
			for b := 0; b < ll; b++ {

				if reflect.TypeOf(v.Index(b).Interface()).Kind() == reflect.Struct {
					structToMap(v.Index(b).Interface(), fp, b+1, ch)
				} else {
					lfp := fp + "[" + strconv.Itoa(b) + "]"
					ch[lfp] = v.Index(b).Interface()

				}

			}

		} else {
			if !(tag == "" || tag == "-") {
				ch[fp] = v.Interface()
			}
		}
	}
}
