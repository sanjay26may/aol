package s2m

import (
	"fmt"
	"testing"
)

type Loc struct {
	Street  interface{} `json:"street_address"`
	Country Country     `json:"country"`
	C       city        `json:"city"`
	Zip     interface{} `json:"zip_code"`
}

type Country struct {
	ID   interface{} `json:"country_id"`
	Name interface{} `json:"country_name"`
}
type city struct {
	ID   interface{} `json:"city_id"`
	Name interface{} `json:"city_name"`
}

type User struct {
	FN      interface{} `json:"first_name"`
	LN      interface{} `json:"last_name"`
	Address Loc         `json:"address"`
	Em      interface{} `json:"email_address"`
	PN      []Loc       `json:"pet_names"`
	Ls      interface{} `json:"last_field"`
	Al      []string    `json:"hobbies"`
}

//var fs map[string]interface{}

func TestS2m(t *testing.T) {
	//	fs = make(map[string]interface{})
	u := User{
		FN: "Sanjay",
		LN: "kumar",
		Address: Loc{
			Street: "location street",
			Country: Country{
				ID:   1234,
				Name: "india",
			},
			// C: City{
			// 	ID:   888,
			// 	Name: "Bangalore",
			// },
			Zip: "560082",
		},
		Em: "sand@yopmail.com",
		PN: []Loc{
			{
				Street: "location street0",
				Country: Country{
					ID:   12340,
					Name: "india0",
				},
				C: city{
					//ID:   8880,
					Name: "Bangalore0",
				},
				Zip: "7777770",
			},
			{
				Street: "location street1",
				Country: Country{
					ID:   12341,
					Name: "india1",
				},
				C: city{
					ID:   8881,
					Name: "Bangalore1",
				},
				Zip: "888881",
			},
		},
		Ls: "this is last field",
		Al: []string{"hobby 1", "hobby 2", "hobby 3"},
	}

	ch := StructToMap(u)

	for k, v := range ch {
		fmt.Println(k, "=", v)

	}

}
