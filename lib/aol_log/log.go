package aollog

import (
	"github.com/go-ozzo/ozzo-log"
)

type Logger struct {
	logger *log.Logger
}

func New(logLevel int, logPath, env string) *Logger {
	var level log.Level
	switch {
	case logLevel == 0:
		level = log.LevelEmergency
	case logLevel == 1:
		level = log.LevelAlert
	case logLevel == 2:
		level = log.LevelCritical
	case logLevel == 3:
		level = log.LevelError
	case logLevel == 4:
		level = log.LevelWarning
	case logLevel == 5:
		level = log.LevelNotice
	case logLevel == 6:
		level = log.LevelInfo
	case logLevel > 6:
		level = log.LevelDebug
	}

	l := log.NewLogger()

	// setting up console target
	t1 := log.NewConsoleTarget()
	t1.MaxLevel = level

	// setitngup file target
	t2 := log.NewFileTarget()
	t2.MaxLevel = level
	t2.FileName = logPath

	if logPath == "" {
		t2.FileName = "app.log"

	}

	l.Targets = append(l.Targets, t1, t2)
	ll := &Logger{}
	ll.logger = l
	return ll
}

func (l *Logger) Open() {

	l.logger.Open()

}
func (l *Logger) Close() {

	l.logger.Close()

}
func (l *Logger) Info(m string) {

	l.logger.Info(m)

}
func (l *Logger) Debug(m string) {
	l.logger.Debug(m)
}
func (l *Logger) Notice(m string) {
	l.logger.Notice(m)
}
func (l *Logger) Warning(m string) {
	l.logger.Warning(m)
}
func (l *Logger) Error(m string) {
	l.logger.Error(m)
}
func (l *Logger) Critical(m string) {
	l.logger.Critical(m)
}
func (l *Logger) Alert(m string) {
	l.logger.Alert(m)
}
func (l *Logger) Emergency(m string) {
	l.logger.Emergency(m)
}
