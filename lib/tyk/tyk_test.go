package tyk

import (
	"fmt"
	"net/http/httptest"
	"testing"
)

func TestMeta(t *testing.T) {

	req := httptest.NewRequest("GET", "http://www.example.com", nil)
	req.Header.Set("x-t-meta", "eyJsYXN0X2NoZWNrIjowLCJhbGxvd2FuY2UiOjEwMDAsInJhdGUiOjEwMDAsInBlciI6NjAsImV4cGlyZXMiOjAsInF1b3RhX21heCI6LTEsInF1b3RhX3JlbmV3cyI6MTUxOTA0MTc1OSwicXVvdGFfcmVtYWluaW5nIjotMSwicXVvdGFfcmVuZXdhbF9yYXRlIjozNjAwLCJhY2Nlc3NfcmlnaHRzIjp7IjdlNTI1OGFmYmFhNjRiOGI3ZjY3N2E3MThjYjQ4OGY5Ijp7ImFwaV9uYW1lIjoiRGVidWcgQVBJIiwiYXBpX2lkIjoiN2U1MjU4YWZiYWE2NGI4YjdmNjc3YTcxOGNiNDg4ZjkiLCJ2ZXJzaW9ucyI6WyJEZWZhdWx0Il0sImFsbG93ZWRfdXJscyI6W119fSwib3JnX2lkIjoiNTY0OTlkOGZjYWE0NWIyZDA1MDAwMDAxIiwib2F1dGhfY2xpZW50X2lkIjoiIiwib2F1dGhfa2V5cyI6bnVsbCwiY2VydGlmaWNhdGUiOiIiLCJiYXNpY19hdXRoX2RhdGEiOnsicGFzc3dvcmQiOiIiLCJoYXNoX3R5cGUiOiIifSwiand0X2RhdGEiOnsic2VjcmV0IjoiIn0sImhtYWNfZW5hYmxlZCI6ZmFsc2UsImhtYWNfc3RyaW5nIjoiIiwiaXNfaW5hY3RpdmUiOmZhbHNlLCJhcHBseV9wb2xpY3lfaWQiOiIiLCJhcHBseV9wb2xpY2llcyI6WyI1YTg2Yjk2NmEyYjIxMzZlOTQ4ZWZkYmEiXSwiZGF0YV9leHBpcmVzIjowLCJtb25pdG9yIjp7InRyaWdnZXJfbGltaXRzIjpudWxsfSwiZW5hYmxlX2RldGFpbF9yZWNvcmRpbmciOmZhbHNlLCJtZXRhX2RhdGEiOnsia2V5MSI6InZhbHVlIDEiLCJ0eWtfZGV2ZWxvcGVyX2lkIjoiNTZhNWRlMzZjYWE0NWIxMDE5MDAwMDAzIiwidHlrX2tleV9yZXF1ZXN0X2ZpZWxkcyI6IntcImFwaV9hcHByb3ZlX2RhdGVcIjpcIjIwMTgvMDEvMTJcIixcImFwaV9yZXF1ZXN0X2RhdGVcIjpcIjIwMTgvMDEvMTJcIixcImNvdW50cnlcIjpcInphXCIsXCJkb21haW5cIjpcIlpBLUgwLTExOTZcIixcImxhc3RuYW1lXCI6XCJrdW1hclwiLFwibmFtZVwiOlwic2FuamF5IGt1bWFyXCIsXCJvcmdhbml6YXRpb25cIjpcIlpBLUgwLTExOTZcIn0iLCJ0eWtfdXNlcl9maWVsZHMiOnsiRmlyc3ROYW1lIjoic2FuZCIsIkxhc3ROYW1lIjoiS3VtYXIifX0sInRhZ3MiOltdLCJhbGlhcyI6InNhbmpheTI2bWF5QGdtYWlsLmNvbSIsImxhc3RfdXBkYXRlZCI6IjE1MTg3Nzg2MjgiLCJpZF9leHRyYWN0b3JfZGVhZGxpbmUiOjAsInNlc3Npb25fbGlmZXRpbWUiOjB9")
	m, err := Meta(req)
	if err != nil {
		fmt.Println("error: ", err)
	} else {
		fmt.Printf("%+v \n", m)

		fmt.Println("apiid: ", m.ApiId())

		fmt.Println("api name: ", m.ApiName())
		fmt.Println("dev_email: ", m.DeveloperEmail())
		fmt.Println("dev id: ", m.DeveloperId())
		fmt.Println("name", m.RequestField("name"))
		fmt.Println("name", m.MetaField("name"))
		fmt.Println("key1", m.MetaField("key1"))
		fmt.Println("Raw meta", m.MetaFields())
		fmt.Printf("Raw meta[key1]: %+v \n", m.MetaField("tyk_user_fields"))
	}

}

//eyJsYXN0X2NoZWNrIjowLCJhbGxvd2FuY2UiOjEwMDAsInJhdGUiOjEwMDAsInBlciI6NjAsImV4cGlyZXMiOjAsInF1b3RhX21heCI6LTEsInF1b3RhX3JlbmV3cyI6MTUxODc4NTI5MCwicXVvdGFfcmVtYWluaW5nIjotMSwicXVvdGFfcmVuZXdhbF9yYXRlIjotMSwiYWNjZXNzX3JpZ2h0cyI6eyI3ZTUyNThhZmJhYTY0YjhiN2Y2NzdhNzE4Y2I0ODhmOSI6eyJhcGlfbmFtZSI6IkRlYnVnIEFQSSIsImFwaV9pZCI6IjdlNTI1OGFmYmFhNjRiOGI3ZjY3N2E3MThjYjQ4OGY5IiwidmVyc2lvbnMiOlsiRGVmYXVsdCJdLCJhbGxvd2VkX3VybHMiOltdfX0sIm9yZ19pZCI6IjU2NDk5ZDhmY2FhNDViMmQwNTAwMDAwMSIsIm9hdXRoX2NsaWVudF9pZCI6IiIsIm9hdXRoX2tleXMiOm51bGwsImNlcnRpZmljYXRlIjoiIiwiYmFzaWNfYXV0aF9kYXRhIjp7InBhc3N3b3JkIjoiIiwiaGFzaF90eXBlIjoiIn0sImp3dF9kYXRhIjp7InNlY3JldCI6IiJ9LCJobWFjX2VuYWJsZWQiOmZhbHNlLCJobWFjX3N0cmluZyI6IiIsImlzX2luYWN0aXZlIjpmYWxzZSwiYXBwbHlfcG9saWN5X2lkIjoiIiwiYXBwbHlfcG9saWNpZXMiOlsiNWE4NmI5NjZhMmIyMTM2ZTk0OGVmZGJhIiwiNTg3NjJlMTJjYWE0NWI3ZWQ4MDAwMDNmIl0sImRhdGFfZXhwaXJlcyI6MCwibW9uaXRvciI6eyJ0cmlnZ2VyX2xpbWl0cyI6bnVsbH0sImVuYWJsZV9kZXRhaWxfcmVjb3JkaW5nIjpmYWxzZSwibWV0YV9kYXRhIjp7ImNvdW50cnkiOiJ1cyIsImZpZWxkMyI6ImZpZWxkIDMgdmFsdWUiLCJuYW1lIjoic2FuamF5In0sInRhZ3MiOltdLCJhbGlhcyI6InNhbmpheWtAYXJ0b2ZsaXZpbmcub3JnIiwibGFzdF91cGRhdGVkIjoiMTUxODc4NTI5MSIsImlkX2V4dHJhY3Rvcl9kZWFkbGluZSI6MCwic2Vzc2lvbl9saWZldGltZSI6MH0
