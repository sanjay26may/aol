// package tyk decodes x-t-meta header from request to respective values
package tyk

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"reflect"
)

type MetaInfo struct {
	MetaData     userMeta                `json:"meta_data"`
	Email        string                  `json:"alias"`
	Tags         []string                `json:"tags"`
	AccessRights map[string]accessRights `json:"access_rights"`
	OrgId        string                  `json:"org_id"`
	PolicyId     string                  `json:"apply_policy_id"`
	IsEmpty      bool
	RawMeta      map[string]interface{}
}

/*type RawMeta struct {
	Fields map[string]interface{} `json:"meta_data"`
}*/
type accessRights struct {
	ApiName string   `json:"api_name"`
	ApiId   string   `json:"api_id"`
	Version []string `json:"versions"`
}
type userMeta struct {
	TykDevId   string      `json:"tyk_developer_id"`
	Rf         interface{} `json:"tyk_key_request_fields"`
	Uf         interface{} `json:"tyk_user_fields"`
	ReqFields  map[string]string
	UserFields map[string]string
}

func Meta(req *http.Request) (*MetaInfo, error) {

	meta := req.Header.Get("x-t-meta")
	info := MetaInfo{}
	rawMeta := struct {
		Fields map[string]interface{} `json:"meta_data"`
	}{}
	if meta == "" {
		info.IsEmpty = true
		return &info, nil
	}

	decMeta, err := base64.StdEncoding.DecodeString(meta)
	if err != nil {
		return &info, err
	}
	err = json.Unmarshal(decMeta, &info)

	if err != nil {
		return &info, err
	}
	err = json.Unmarshal(decMeta, &rawMeta)
	info.RawMeta = rawMeta.Fields
	info.MetaData.ReqFields = getMapvalue(info.MetaData.Rf)
	info.MetaData.UserFields = getMapvalue(info.MetaData.Uf)
	return &info, nil
}

func (m *MetaInfo) ApiName() string {
	n := ""
	for _, v := range m.AccessRights {
		n = v.ApiName
	}
	return n
}

func (m *MetaInfo) ApiId() string {
	n := ""
	for _, v := range m.AccessRights {
		n = v.ApiId
	}
	return n
}

func (m *MetaInfo) RequestField(s string) string {
	return m.MetaData.ReqFields[s]

}
func (m *MetaInfo) RequestFields() map[string]string {
	return m.MetaData.ReqFields

}
func (m *MetaInfo) UserField(s string) string {
	return m.MetaData.UserFields[s]

}
func (m *MetaInfo) UserFields() map[string]string {
	return m.MetaData.UserFields

}
func (m *MetaInfo) DeveloperId() string {
	return m.MetaData.TykDevId
}
func (m *MetaInfo) DeveloperEmail() string {
	return m.Email
}

func (m *MetaInfo) MetaFields() map[string]interface{} {
	return m.RawMeta
}
func (m *MetaInfo) MetaField(s string) interface{} {
	return m.RawMeta[s]
}

func getMapvalue(i interface{}) map[string]string {
	m := make(map[string]string)
	if i == nil {
		return m
	}
	t := reflect.TypeOf(i)

	if t.Kind() == reflect.String {
		s := i.(string)
		json.Unmarshal([]byte(s), &m)
		return m
	}

	if t.Kind() == reflect.Map {
		mi := i.(map[string]interface{})
		for k, v := range mi {
			m[k] = v.(string)
		}
		return m
	}

	return m

}
