package jsvalidator

import (
	"aol/lib/validator/country"
	"errors"
	"github.com/metakeule/fmtdate"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func ValidateDataType(v, dt, fromat, seperator string) (error, []interface{}) {
	if fromat == "" {
		fromat = "YYYY/MM/DD hh:mm:ss"
	}
	if seperator == "" {
		seperator = ","
	}

	var e error
	var val interface{}
	r := make([]interface{}, 0)

	switch dt {
	case "string", "istring":
		val, e = v, nil
		r = append(r, val)

	case "int64":
		val, e = Int64(v)
		r = append(r, val)
	case "int64-unsigned":
		val, e = UnsignedInt64(v)
		r = append(r, val)
	case "int32":
		val, e = Int32(v)
		r = append(r, val)
	case "int16":
		val, e = Int16(v)
		r = append(r, val)
	case "int8":
		val, e = Int8(v)
		r = append(r, val)
	case "int":
		val, e = Int(v)
		r = append(r, val)
	// list type
	case "int_list":
		r, e = CsvInt(v, seperator, false)
	case "email_list":
		r, e = CsvEmail(v, seperator, false)
	case "id_tring_list":
		r, e = CsvIdString(v, seperator, false)
	case "float_list":
		r, e = CsvFloat(v, seperator, false)
	case "date_string_list":
		r, e = CsvDateString(v, fromat, seperator, false)
	// set type
	case "int_set":
		r, e = CsvInt(v, seperator, true)
	case "email_set":
		r, e = CsvEmail(v, seperator, true)
	case "id_string_set":
		r, e = CsvIdString(v, seperator, true)
	case "float_set":
		r, e = CsvFloat(v, seperator, true)
	case "date_string_set":
		r, e = CsvDateString(v, fromat, seperator, true)
	case "number":
		val, e = Int64(v)
		r = append(r, val)
	case "float":
		val, e = Float64(v)
		r = append(r, val)
	case "id_string":
		val, e = IdString(v)
		r = append(r, val)

	case "email", "mail":
		val, e = Email(v)
		r = append(r, val)
	case "date":

		val, e = Date(v, fromat)
		r = append(r, val)
	case "date_string":
		val, e = DateString(v, fromat)
		r = append(r, val)

	case "iso2country":

		r = append(r, v)
		b := country.IsIsoAlpha2(v)
		if !b {

			e = errors.New("invalid country code:" + v)
		}

	}
	return e, r
}

func CsvInt(s, sep string, set bool) ([]interface{}, error) {
	var (
		r []interface{}
		e error
	)
	temp := make(map[string]bool)
	a := strings.Split(strings.Trim(s, sep), sep)
	eMsg := ""
	for _, val := range a {
		v, ee := Int64(val)
		if ee != nil {
			eMsg += ee.Error()
		}
		if set {
			if !temp[val] {
				r = append(r, v)
			}
		} else {
			r = append(r, v)
		}
		temp[val] = true

	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}

	return r, e
}

func CsvFloat(s, sep string, set bool) ([]interface{}, error) {
	var (
		r []interface{}
		e error
	)
	temp := make(map[string]bool)
	a := strings.Split(strings.Trim(s, sep), sep)
	eMsg := ""
	for _, val := range a {
		v, ee := Float64(val)
		if ee != nil {
			eMsg += ee.Error()
		}
		if set {
			if !temp[val] {
				r = append(r, v)
			}
		} else {
			r = append(r, v)
		}
		temp[val] = true
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}

	return r, e
}

func CsvEmail(s, sep string, set bool) ([]interface{}, error) {
	var (
		r []interface{}
		e error
	)
	temp := make(map[string]bool)
	a := strings.Split(strings.Trim(s, sep), sep)
	eMsg := ""
	for _, val := range a {
		v, ee := Email(val)
		if ee != nil {
			eMsg += ee.Error()
		}
		if set {
			if !temp[val] {
				r = append(r, v)
			}
		} else {
			r = append(r, v)
		}
		temp[val] = true

	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	//r = s
	return r, e
}

func CsvIdString(s, sep string, set bool) ([]interface{}, error) {
	var (
		r []interface{}
		e error
	)
	temp := make(map[string]bool)
	a := strings.Split(strings.Trim(s, sep), sep)
	eMsg := ""
	for _, val := range a {
		v, ee := IdString(val)
		if ee != nil {
			eMsg += ee.Error()
		}
		if set {
			if !temp[val] {
				r = append(r, v)
			}
		} else {
			r = append(r, v)
		}
		temp[val] = true
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}

	return r, e
}
func CsvDateString(s, format, sep string, set bool) ([]interface{}, error) {

	var (
		r []interface{}
		e error
	)
	temp := make(map[string]bool)
	a := strings.Split(strings.Trim(s, sep), sep)
	eMsg := ""
	for _, val := range a {
		v, ee := DateString(val, format)
		if ee != nil {
			eMsg += ee.Error()
		}
		if set {
			if !temp[val] {
				r = append(r, v)
			}
		} else {
			r = append(r, v)
		}
		temp[val] = true
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}

	return r, e

	/*d, e := Date(s, format)
	if e != nil {
		return s, e
	}
	return FormatDate(format, d), nil*/
}
func UnsignedInt64(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 64)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	if i < 0 {
		e = errors.New("-ve numbers are not allowed: " + s)
	}
	/*if s[0:1] == "+" || s[0:1] == "-" {
		e = errors.New("+ or - sign is  not allowed: " + s)
	}*/
	return i, e
}
func Int64(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 64)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int32(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 32)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int16(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 16)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int8(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 8)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 0)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}

func Float64(s string) (float64, error) {
	i, e := strconv.ParseFloat(s, 64)
	if e != nil {
		e = errors.New("invalid float format: " + s)
	}
	return i, e
}
func Float32(s string) (float64, error) {
	i, e := strconv.ParseFloat(s, 32)
	if e != nil {
		e = errors.New("invalid float format: " + s)
	}
	return i, e
}

func Email(s string) (string, error) {
	Re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if Re.MatchString(s) {
		return s, nil
	}
	return "", errors.New("invalid email format: " + s)
}

func IdString(s string) (string, error) {
	Re := regexp.MustCompile(`^[A-Za-z0-9]{2,}$`)
	if Re.MatchString(s) {
		return s, nil
	}
	return "", errors.New("invalid format: " + s)
}

func Date(s, format string) (time.Time, error) {
	date := s

	d, err := fmtdate.Parse(format, date)
	if err != nil {
		err = errors.New("invalid date: " + s)
		return d, err
	}

	start := strings.Index(format, "D")
	last := strings.LastIndex(format, "D") + 1

	origDay := s[start:last]
	day := d.Day()

	firstChar := origDay[0:1]
	if firstChar == "0" {
		origDay = origDay[1:]
	}
	stringDay := strconv.Itoa(day)
	if stringDay != origDay {
		err = errors.New("invalid date: " + s)
	}
	//
	//handling the case where date is shifted to next month without generating error
	// eg 31 Feb 2016 will be parsed to 2nd March 2016
	return d, err
}

func DateString(s, format string) (string, error) {
	d, e := Date(s, format)
	if e != nil {
		return s, e
	}
	return FormatDate(format, d), nil
}

func FormatDate(format string, d time.Time) string {
	return fmtdate.Format(format, d)
}
