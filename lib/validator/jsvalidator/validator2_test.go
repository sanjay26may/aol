package jsvalidator

import (
	//"aol/lib/validator/validaror2"
	//"bytes"
	"encoding/json"
	"fmt"
	//"net/http/httptest"
	"testing"
)

// type ReqFor struct {
// 	Field    string        `json:"field"`
// 	Value    []interface{} `json:"value"`
// 	AnyValue bool          `json:"any_value"`
// }
// type FConfig struct {
// 	DataType     string   `json:"data_type"`
// 	Required     bool     `json:"required"`
// 	Enum         bool     `json:"enum"`
// 	EnumList     []string `json:"enum_list"`
// 	Conditional  bool     `json:"conditional"`
// 	RequiredFor  []ReqFor `json:"required_for"`
// 	Length       int64    `json:"length"`
// 	RequiredWith []string
// }

type Loc struct {
	Street  interface{} `json:"street_address"`
	Country Country     `json:"country"`
	C       city        `json:"city"`
	Zip     interface{} `json:"zip_code"`
}

type Country struct {
	ID   interface{} `json:"country_id"`
	Name interface{} `json:"country_name"`
}
type city struct {
	ID   interface{} `json:"city_id"`
	Name interface{} `json:"city_name"`
}

type User struct {
	FN      interface{} `json:"first_name"`
	LN      interface{} `json:"last_name"`
	Address Loc         `json:"address"`
	Em      interface{} `json:"email_address"`
	PN      []Loc       `json:"pet_names"`
	Ls      interface{} `json:"last_field"`
	Al      []string    `json:"hobbies"`
}

func TestOutput(t *testing.T) {

	d := `{
    "email_address":{
        "data_type":"email",
				"check_disposable":true,
				"required":true
       
    },
    "first_name":{
			"data_type":"string",
			"required":true
    		

		},
		"address.city.city_id":{
			"data_type":"number",
			"required":true
		},
		"pet_names[*].city.city_id":{
			"data_type":"number",
			"required":true
		}

}`

	errTemplate := `{
	"required":{
		"type":"Business error",
		"title":"MissingRequiredField",
		"desc":"Value for {{ .fieldName }} is missing"
	},
	"data_type-date":{
		"type":"Data error",
		"title":"InvalidDate",
		"desc":"Format provided for {{ .fieldName }} is invalid or invalid date {{.value }} found. Expected format is {{ .format }}"
	},
	"required_with":{
		"type":"Business error",
		"title":"MissingConditionalField",
		"desc":"field {{ .fieldName }} is required with {{ .requiredWith}} "
	},
	"value_set":{
		"type":"Data error",
		"title":"InvalidValue",
		"desc":"field {{ .fieldName }} can have [{{ .valueSet }}] values. Value provided is {{ .value}}"
	},
	"length":{
		"type":"Business error",
		"title":"InvalidFieldSize",
		"desc":"length of value provided for {{ .fieldName }} exceeds the allowed limit of {{ .length }} "
	},
	"data_type-number":{
		"type":"Data error",
		"title":"InvalidDataType",
		"desc":"allowed data type for {{ .fieldName}} is {{ .dataType }}. Provided value is {{ .value }} "
	},
	"check_disposable":{
		"type":"Data error",
		"title":"InvalidValue",
		"desc":"{{ .value }} not allowed for {{ .fieldName}} "
	}
}`

	/*
		date: not in future
		not in past


	*/
	u := User{
		FN: "Sanjay",
		LN: "kumar",
		Address: Loc{
			Street: "location street",
			Country: Country{
				ID:   1234,
				Name: "india",
			},
			C: city{
				ID:   888,
				Name: "Bangalore",
			},
			Zip: "560082",
		},
		Em: "sand@yopmail.com",
		PN: []Loc{
			{
				Street: "location street0",
				Country: Country{
					ID:   12340,
					Name: "india0",
				},
				C: city{
					ID:   8880,
					Name: "Bangalore0",
				},
				Zip: "7777770",
			},
			{
				Street: "location street1",
				Country: Country{
					ID:   12341,
					Name: "india1",
				},
				C: city{
					ID:   "sanj",
					Name: "Bangalore1",
				},
				Zip: "888881",
			},
		},
		Ls: "this is last field",
		Al: []string{"hobby 1", "hobby 2", "hobby 3"},
	}

	var fields ConfigStructure
	err := json.Unmarshal([]byte(d), &fields)
	if err != nil {
		fmt.Println(err)
		fmt.Println(err)
	}

	et := make(map[string]map[string]string)

	err = json.Unmarshal([]byte(errTemplate), &et)
	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println(Fields)

	result := Validate(u, fields)
	/*for k, v := range result.Fields {
		&fmt.Println("field_name", k)
		fmt.Println("data type", v.DataType)
		fmt.Println("raw value", v.Raw)
		fmt.Println("parsed value", v.Parsed)
		fmt.Println("error", v.Err)
		fmt.Println("param present", v.ParamPresent)
		fmt.Println("************************************************")

		//fmt.Println(v.Missing)
	}*/

	//fmt.Println(errorList)
	if len(result.AllErrors) > 0 {
		fmt.Println("validation errors with tempelete")
		fmt.Println("--------------------------------")
		pe := result.FormatError(et)
		fmt.Printf("%+v\n", pe)

		fmt.Println("")
		fmt.Println("validation errors without tempelete")
		fmt.Println("-----------------------------------")

		for _, v := range result.AllErrors {
			fmt.Println(v)
		}
	} else {
		fmt.Println("validation success")
	}
	//t.Log(op)

}
