package validator

import (
	"aol/lib/validator/country"
	"errors"
	//"fmt"
	"github.com/metakeule/fmtdate"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func Validate(s string, t string, mandatory bool) (interface{}, error) {
	var (
		r interface{}
		e error
	)
	t = strings.ToLower(t)
	if mandatory {
		if s == "" {
			r, e = s, errors.New("missing mandatory field")
		}
	}
	if s != "" {

		switch t {
		case "string", "istring":
			r, e = s, nil
		case "int64":
			r, e = Int64(s)
		case "int32":
			r, e = Int32(s)
		case "int16":
			r, e = Int16(s)
		case "int8":
			r, e = Int8(s)
		case "int":
			r, e = Int(s)
		case "number":
			r, e = Int64(s)
		case "float":
			r, e = Float64(s)
		case "email", "mail":
			r, e = Email(s)
		case "date":
			r, e = Date(s, "YYYY/MM/DD")
		case "idString":
			r, e = IdString(s)
		case "dateString":
			r, e = DateString(s, "YYYY/MM/DD")
		case "csvDateString":
			r, e = CsvDateString(s, "YYYY/MM/DD")
		case "csvIdString":
			r, e = CsvIdString(s)
		case "csvint":
			r, e = CsvInt(s)
		case "csvfloat":
			r, e = CsvFloat(s)
		case "csvemail":
			r, e = CsvEmail(s)
		case "iso2country":
			b := country.IsIsoAlpha2(s)
			r = s
			if !b {

				e = errors.New("invalid country code:" + s)
			}
		default:
			r, e = "", errors.New("missing_format")

		}

	}
	return r, e

}
func CsvInt(s string) (string, error) {
	var (
		r string
		e error
	)
	a := strings.Split(strings.Trim(s, ","), ",")
	eMsg := ""
	for _, val := range a {
		_, ee := Int64(val)
		if ee != nil {
			eMsg += ee.Error()
		}
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	r = s
	return r, e
}

func CsvFloat(s string) (string, error) {
	var (
		r string
		e error
	)
	a := strings.Split(strings.Trim(s, ","), ",")
	eMsg := ""
	for _, val := range a {
		_, ee := Float64(val)
		if ee != nil {
			eMsg += ee.Error()
		}
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	r = s
	return r, e
}

func CsvEmail(s string) (string, error) {
	var (
		r string
		e error
	)
	a := strings.Split(strings.Trim(s, ","), ",")
	eMsg := ""
	for _, val := range a {
		_, ee := Email(val)
		if ee != nil {
			eMsg += ee.Error()
		}
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	r = s
	return r, e
}

func CsvIdString(s string) (string, error) {
	var (
		r string
		e error
	)
	a := strings.Split(strings.Trim(s, ","), ",")
	eMsg := ""
	for _, val := range a {
		_, ee := IdString(val)
		if ee != nil {
			eMsg += ee.Error()
		}
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	r = s
	return r, e
}
func Int64(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 64)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int32(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 32)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int16(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 16)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int8(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 8)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}
func Int(s string) (int64, error) {
	i, e := strconv.ParseInt(s, 10, 0)
	if e != nil {
		e = errors.New("invalid integer format: " + s)
	}
	return i, e
}

func Float64(s string) (float64, error) {
	i, e := strconv.ParseFloat(s, 64)
	if e != nil {
		e = errors.New("invalid float format: " + s)
	}
	return i, e
}
func Float32(s string) (float64, error) {
	i, e := strconv.ParseFloat(s, 32)
	if e != nil {
		e = errors.New("invalid float format: " + s)
	}
	return i, e
}

func Email(s string) (string, error) {
	Re := regexp.MustCompile(`\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3}`)
	if Re.MatchString(s) {
		return s, nil
	}
	return "", errors.New("invalid email format: " + s)
}

func IdString(s string) (string, error) {
	Re := regexp.MustCompile(`^[A-Za-z0-9]{2,}$`)
	if Re.MatchString(s) {
		return s, nil
	}
	return "", errors.New("invalid format: " + s)
}

func Date(s, format string) (time.Time, error) {
	date := s

	d, err := fmtdate.Parse(format, date)
	if err != nil {
		err = errors.New("invalid date: " + s)
		return d, err
	}

	start := strings.Index(format, "D")
	last := strings.LastIndex(format, "D") + 1

	origDay := s[start:last]
	day := d.Day()

	firstChar := origDay[0:1]
	if firstChar == "0" {
		origDay = origDay[1:]
	}
	stringDay := strconv.Itoa(day)
	if stringDay != origDay {
		err = errors.New("invalid date: " + s)
	}
	//
	//handling the case where date is shifted to next month without generating error
	// eg 31 Feb 2016 will be parsed to 2nd March 2016
	return d, err
}

func DateString(s, format string) (string, error) {
	d, e := Date(s, format)
	if e != nil {
		return s, e
	}
	return FormatDate(format, d), nil
}

func CsvDateString(s, format string) (string, error) {

	var (
		r string
		e error
	)
	a := strings.Split(strings.Trim(s, ","), ",")
	eMsg := ""
	for _, val := range a {
		_, ee := DateString(val, format)
		if ee != nil {
			eMsg += ee.Error()
		}
	}
	if eMsg != "" {
		e = errors.New(eMsg)
	}
	r = s
	return r, e

	/*d, e := Date(s, format)
	if e != nil {
		return s, e
	}
	return FormatDate(format, d), nil*/
}

func FormatDate(format string, d time.Time) string {
	return fmtdate.Format(format, d)
}

func ValidateFields(f []map[string]interface{}, u url.Values) (map[string]interface{}, error, []string) {
	var isError bool

	msg := make([]string, 0)
	missingMandatory := make([]string, 0)
	cpvError := make([]string, 0)

	cmfError := make(map[string]string, 0)
	op := make(map[string]interface{}, 0)

	for _, v := range f {
		var r interface{}
		var err error
		f := v["f"].(string)                  // field name
		m := strings.ToLower(v["m"].(string)) // mandatory y,n,conditional etc
		dt := v["dt"].(string)                // data type of field
		ot := ""
		if v["ot"] != nil {
			ot = v["ot"].(string) // other type that can indicate type pf data present in ov field
		}

		ov := make([]interface{}, 0)
		if v["ov"] != nil {
			ov = v["ov"].([]interface{})
		}

		lv := getListvalue(ov)

		sAllowedLength := "0"
		sListSize := "0"
		if v["f_size"] != nil {
			sAllowedLength = v["f_size"].(string)

		}
		al, _ := strconv.ParseInt(sAllowedLength, 10, 64)
		allowedLength := int(al)

		if v["l_size"] != nil {
			sListSize = v["l_size"].(string)

		}
		ls, _ := strconv.ParseInt(sListSize, 10, 64)
		listSize := int(ls)

		// for every field we will check proper validation
		/*err := handeValidation(f, u.Get(f), m, dt, ot, lv)
		if err != nil {
			flag = err
			msg = append(msg, err.Error())
		}*/

		// checking mandatory empty fields
		tv := strings.Trim(u.Get(f), " ")
		v := u.Get(f)

		if (m == "y" || m == "ylv") && tv == "" {
			missingMandatory = append(missingMandatory, f)
			isError = true
		} else if m == "y" && tv != "" {
			//data type validation
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
		} else if m == "ylv" {
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
			if !foundInList(v, lv, dt) {
				isError = true
				msg = append(msg, f+" can only have one of these value(s): ("+strings.Join(lv, ",")+")")
			}

		} else if m == "csvylv" {
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
			if !allInList(v, lv, dt) {
				isError = true
				msg = append(msg, f+" can only have one of these value(s): ("+strings.Join(lv, ",")+")")
			}

		} else if m == "cpv" {
			parentValue := u.Get(ot)
			if foundInList(parentValue, lv, dt) {
				if tv == "" {
					isError = true
					cpvError = append(cpvError, "field "+f+" should not be empty when "+ot+" is ("+strings.Join(lv, " or ")+")")
				} else {
					r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
					if err != nil {
						isError = true
						msg = append(msg, err.Error())
					}
				}

			}

		} else if m == "n" && tv != "" {
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
		} else if m == "nlv" && tv != "" {
			if !foundInList(v, lv, dt) {
				isError = true
				msg = append(msg, f+" can only have one of these value(s): ("+strings.Join(lv, ",")+")")
			}
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
		} else if m == "csvnlv" && tv != "" {
			if !allInList(v, lv, dt) {
				isError = true
				msg = append(msg, f+" can only have one of these value(s): ("+strings.Join(lv, ",")+")")
			}
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
		} else if m == "cmf" {
			allEmpty := true
			if tv == "" {

				for _, v := range lv {
					if strings.Trim(u.Get(v), " ") != "" {
						allEmpty = false

					}

				}
			} else {
				allEmpty = false
				r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
				if err != nil {
					isError = true
					msg = append(msg, err.Error())
				}
			}

			if allEmpty {
				isError = true
				cmfError[strings.Join(lv, "_")] = "at least one of (" + strings.Join(lv, ",") + ") should not be empty"

			}
		} else if m == "comb_mand" {
			r, err = formatValidator(f, v, dt, ot, lv, allowedLength, listSize)
			if err != nil {
				isError = true
				msg = append(msg, err.Error())
			}
			allTrue := true
			arrField := make([]string, 0)

			for _, v := range ov {
				fieldName := v.(string)
				arrField = append(arrField, fieldName)
				iFlag := true
				if u.Get(fieldName) == "" {
					iFlag = false
				}
				if (!allTrue && !iFlag) || (allTrue && iFlag) {
					allTrue = true
				} else {
					allTrue = false
				}

			}
			if !allTrue {
				isError = true
				msg = append(msg, "all fields("+strings.Join(lv, ",")+") together must have values")
			}
		}
		if r != nil {
			op[f] = r
		} else {
			op[f] = getNil(dt)
		}

	}
	if len(missingMandatory) > 0 {
		msg = append(msg, "missing mandatory field(s): ("+strings.Join(missingMandatory, ",")+")")
	}
	if len(cpvError) > 0 {
		msg = append(msg, cpvError...)
	}
	if len(cmfError) > 0 {
		for _, v := range cmfError {
			msg = append(msg, v)
		}
	}
	if isError {
		return op, errors.New("input field validation failed"), msg
	}

	return op, nil, msg
}
func getListvalue(av []interface{}) []string {
	a := make([]string, 0)
	for _, v := range av {
		a = append(a, v.(string))
	}
	return a
}

func formatValidator(f, v, dt, ot string, ov []string, allowedLength, listSize int) (interface{}, error) {
	//removing leading trailing spaces to check for mandatory
	var e error
	var r interface{}
	var csvType bool
	switch dt {
	case "string", "istring":
		r, e = v, nil

	case "int64":
		r, e = Int64(v)
	case "int32":
		r, e = Int32(v)
	case "int16":
		r, e = Int16(v)
	case "int8":
		r, e = Int8(v)
	case "int":
		r, e = Int(v)
	case "csvint":
		csvType = true
		r, e = CsvInt(v)
	case "csvemail", "csvmail":
		csvType = true
		r, e = CsvEmail(v)
	case "csvIdString":
		csvType = true
		r, e = CsvIdString(v)
	case "number":
		r, e = Int64(v)
	case "float":
		r, e = Float64(v)
	case "idString":
		r, e = IdString(v)
	case "csvfloat":
		csvType = true
		r, e = CsvFloat(v)
	case "email", "mail":
		r, e = Email(v)
	case "date":
		if len(ov) > 0 {
			r, e = Date(v, ov[0])
		} else {
			r, e = Date(v, "YYYY/MM/DD hh:mm:ss")
		}
	case "dateString":
		if len(ov) > 0 {
			r, e = DateString(v, ov[0])
		} else {
			r, e = DateString(v, "YYYY/MM/DD hh:mm:ss")
		}
	case "csvDateString":
		csvType = true
		if len(ov) > 0 {
			r, e = CsvDateString(v, ov[0])
		} else {
			r, e = CsvDateString(v, "YYYY/MM/DD hh:mm:ss")
		}
	case "iso2country":
		r = v
		b := country.IsIsoAlpha2(v)
		if !b {

			e = errors.New("invalid country code:" + v)
		}

	}

	if allowedLength != 0 && !csvType {
		if allowedLength < len(v) {
			e = errors.New("only " + strconv.Itoa(allowedLength) + " characters are allowed")
		}
	}
	if csvType {
		chunks := strings.Split(strings.Trim(v, ","), ",")
		l := len(chunks)
		if l > listSize && listSize > 0 {
			e = errors.New("only " + strconv.Itoa(listSize) + " csv values are allowed")

		}
		if allowedLength != 0 && e == nil {
			for _, chunk := range chunks {
				if len(chunk) > allowedLength {
					e = errors.New("one of csv item in " + f + " has more than " + strconv.Itoa(allowedLength) + " character")
					break
				}
			}
		}
	}
	if e != nil {
		e = errors.New(e.Error() + " for parameter (" + f + ")")
	}

	return r, e
}

func foundInList(s string, l []string, dt string) bool {
	if dt == "istring" {
		s = strings.ToLower(s)
	}
	for _, v := range l {
		vv := strings.ToLower(v)
		if s == vv {
			return true
		}
	}
	return false
}

func allInList(s string, l []string, dt string) bool {
	lists := strings.Split(s, ",")
	allTrue := true
	for _, v := range lists {
		allTrue = allTrue && foundInList(v, l, dt)
		if !allTrue {
			return allTrue
		}

	}
	return allTrue
}

func getNil(dt string) (op interface{}) {

	switch dt {
	case "string", "istring":
		op = ""
	case "int64":
		op = int64(0)
	case "int32":
		op = int32(0)
	case "int16":
		op = int16(0)
	case "int8":
		op = int8(0)
	case "int":
		op = int(0)
	case "number":
		op = 0
	case "float":
		op = 0.00
	case "email", "mail":
		op = ""
	case "date":
		op = time.Time{}
	}
	return op
}
