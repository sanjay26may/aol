// data types are int,float,string,date,date_string,email,iso2country,id_string,

// int_list,float_list,date_string_list,email_list,id_string_list  this data type is used for list of basic  data types
// int_set, float_set, date_string_set, email_set,id_string_set this will validate and return unique value
// for list or set data type "seperator" can be used, default seperator is ","(comma)

// length,min_len,max_len checks for length of input value

// list_length, list_min_len, list_max_len checks for number of input values sepereted by seperator..
// 		in case of "set" type length is calculated after removing duplicated

// for data type email,email_list and email_set check_disposable=true/false can be set, it will check if email
// 		ids are disposable or not if check_disposable=true and email=asdfa@yopmail.com it will return invalid email
// 		because yopmail.com domain is used for disposable emails. curruently we have ~400 listed domains

// for conditional mandatory fields we can use following attributes
// 		required_for
// 		required_with
// 		required_any_one

package validator2

import (
	//"errors"
	"bytes"
	"errors"
	"fmt"
	"math"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"
)

type Output struct {
	Raw          string
	Parsed       []interface{}
	Err          map[string]error
	ParamPresent bool
	HasValue     bool
	DataType     string
	Conf         ConfStructure
}

type Result struct {
	Fields        map[string]Output
	ErrorsByType  map[string]map[string]error
	ErrorsByField map[string]map[string]error
	AllErrors     []error
}

type ConfStructure struct {
	DataType    string   `json:"data_type"`
	Required    bool     `json:"required"`
	Enum        bool     `json:"enum"`
	EnumList    []string `json:"value_set"`
	Conditional bool     `json:"conditional"`
	RequiredFor []struct {
		Field    string   `json:"field"`
		Value    []string `json:"value"`
		AnyValue bool     `json:"any_value"`
	} `json:"required_for"`
	Length            int64    `json:"length"`
	MinLen            int64    `json:"min_len"`
	MaxLen            int64    `json:"max_len"`
	RequiredWith      []string `json:"required_with"`
	RequiredAnyOne    []string `json:"required_any_one"`
	RequiredEitherOne []string `json:"required_either_one"`
	DateFormat        string   `json:"format"`
	Seperator         string   `json:"seperator"`
	CheckDisposable   bool     `json:"check_disposable"`
	ListMinLen        int      `json:"list_min_len"`
	ListMaxLen        int      `json:"list_max_len"`
	ListLength        int      `json:"list_length"`
	CaseSensitive     bool     `json:"case_sensitive"`
	IsFutureDate      bool     `json:"future_date"`
	IsPastDate        bool     `json:"past_date"`
	DateMaxDiff       int64    `json:"day_diff"`
	Default           string   `json:"default"`
}

type ConfigStructure map[string]ConfStructure

func Validate(req *http.Request, fields ConfigStructure) Result {

	op := make(map[string]Output)
	masterErrors := make(map[string]map[string]map[string]error)
	masterErrors["by_type"] = make(map[string]map[string]error)
	masterErrors["by_field"] = make(map[string]map[string]error)
	allErrors := make([]error, 0)

	//mandatoryFields := make([]string, 0)
	//condnMandErro := make(map[string]error)
	//var err error
	for k, v := range fields {

		// keeping default seperator comma (,)
		if v.Seperator == "" {
			v.Seperator = ","
		}

		// check if that field is empty
		r := Output{}
		r.Conf = v
		r.Err = make(map[string]error)

		r.ParamPresent, r.Raw = fromValue(req, k)

		// handling default value
		if r.Raw == "" && v.Default != "" {
			r.ParamPresent = true
			r.Raw = v.Default
		}

		// mandatory checke
		if v.Required && r.Raw == "" {
			//mandatoryFields = append(mandatoryFields, k)
			//r.Err = append(r.Err, errors.New(k+" is mandatory field"))
			e := errors.New("[" + k + "] is mandatory field")
			r.Err["required"] = e
			masterErrors["by_type"]["required"] = map[string]error{k: e}
			allErrors = append(allErrors, e)

		}
		r.DataType = v.DataType

		// conditional checks

		if v.Conditional && len(v.RequiredAnyOne) > 0 {
			err, _ := checkRequiredAnyOne(req, v.RequiredAnyOne)
			if err != nil {
				//r.Err = append(r.Err, err)
				r.Err["required_any_one"] = err
				masterErrors["by_type"]["required_any_one"] = map[string]error{k: err}
				allErrors = append(allErrors, err)
				//condnMandErro[field_key] = err
			}

		}

		if v.Conditional && len(v.RequiredEitherOne) > 0 {
			err, _ := checkRequiredEitherOne(req, v.RequiredEitherOne)
			if err != nil {
				//r.Err = append(r.Err, err)
				r.Err["required_either_one"] = err
				masterErrors["by_type"]["required_either_one"] = map[string]error{k: err}
				allErrors = append(allErrors, err)
				//condnMandErro[field_key] = err
			}

		}

		if v.Conditional && len(v.RequiredWith) > 0 {
			err := checkRequiredWith(k, req, v.RequiredWith)
			if err != nil {
				r.Err["required_with"] = err
				masterErrors["by_type"]["required_with"] = map[string]error{k: err}
				allErrors = append(allErrors, err)
			}

		}
		if v.Conditional && len(v.RequiredFor) > 0 {
			err := checkRequiredFor(k, req, v.CaseSensitive, v.RequiredFor)
			if err != nil {
				r.Err["required_for"] = err
				masterErrors["by_type"]["required_for"] = map[string]error{k: err}
				allErrors = append(allErrors, err)
			}
		}

		// data type validation [covers enum list also]

		if r.Raw != "" {
			r.HasValue = true
			err, pv := ValidateDataType(r.Raw, v.DataType, v.DateFormat, v.Seperator)
			if err == nil {
				r.Parsed = pv
				// disposable email domain check
				if (v.CheckDisposable) && (strings.ToLower(v.DataType) == "email" || strings.ToLower(v.DataType) == "email_set" || strings.ToLower(v.DataType) == "email_list") {
					for _, v := range r.Parsed {
						if IsDisposableEmail(v.(string)) {
							//r.Err = append(r.Err, errors.New(v.(string)+" is disposable email"))
							e := errors.New(v.(string) + " is disposable email")
							r.Err["check_disposable"] = e
							masterErrors["by_type"]["check_disposable"] = map[string]error{k: e}
							allErrors = append(allErrors, e)
						}
					}
				}

				if v.DataType == "date" {
					// check past date
					if v.IsPastDate {
						isPast, _ := isPastDate(r.Parsed)
						//isPast, diff := isPastDate(r.Parsed)
						//fmt.Println(isPast, diff, v.DateMaxDiff)
						/*if (isPast && v.DateMaxDiff==0){

						}*/
						//if !(isPast && (diff <= v.DateMaxDiff)) {
						if !(isPast) {
							err := errors.New("future date is not allowed for parameter " + k)
							if v.DateMaxDiff != 0 && isPast {
								err = errors.New("date before " + strconv.Itoa(int(v.DateMaxDiff)) + " days from current date is not allowed for " + k)
							}

							r.Err["past_date"] = err
							masterErrors["by_type"]["past_date"] = map[string]error{k: err}
							allErrors = append(allErrors, err)

						}
					}
					// check futture date
					if v.IsFutureDate {
						isPast, _ := isPastDate(r.Parsed)
						//isPast, diff := isPastDate(r.Parsed)
						//fmt.Println(isPast, diff, v.DateMaxDiff)

						if isPast {
							err := errors.New("past date is not allowed for parameter " + k)
							if v.DateMaxDiff != 0 && !isPast {
								err = errors.New("date after " + strconv.Itoa(int(v.DateMaxDiff)) + " days from current date is not allowed for " + k)
							}
							r.Err["past_date"] = err
							masterErrors["by_type"]["past_date"] = map[string]error{k: err}
							allErrors = append(allErrors, err)

						}
					}
				}

			} else {
				e := "[" + k + "] " + err.Error()
				err = errors.New(e)
				r.Err["data_type-"+v.DataType] = err
				masterErrors["by_type"]["data_type-"+v.DataType] = map[string]error{k: err}
				allErrors = append(allErrors, err)
				//r.Err = append(r.Err, err)
			}

			// min length test
			if v.MinLen != 0 {
				err := checkMinLength(r.Raw, v.DataType, v.Seperator, k, v.MinLen)
				if err != nil {
					//r.Err = append(r.Err, err)
					r.Err["min_len"] = err
					masterErrors["by_type"]["min_len"] = map[string]error{k: err}
					allErrors = append(allErrors, err)
				}
			}
			// max length test
			if v.MaxLen != 0 {
				err := checkMaxLength(r.Raw, v.DataType, v.Seperator, k, v.MaxLen)
				if err != nil {
					//r.Err = append(r.Err, err)
					r.Err["max_len"] = err
					masterErrors["by_type"]["max_len"] = map[string]error{k: err}
					allErrors = append(allErrors, err)
				}
			}
			// fixed length test
			if v.Length != 0 {
				err := checkLength(r.Raw, v.DataType, v.Seperator, k, v.Length)
				if err != nil {
					//r.Err = append(r.Err, err)
					r.Err["length"] = err
					masterErrors["by_type"]["length"] = map[string]error{k: err}
					allErrors = append(allErrors, err)

				}
			}
			// list min length check
			if (v.ListMinLen != 0) && (len(r.Parsed) < v.ListMinLen) {
				//r.Err = append(r.Err, errors.New("list size should not be less than "+strconv.Itoa(v.ListMinLen)+" for "+k))
				e := errors.New("list size should not be less than " + strconv.Itoa(v.ListMinLen) + " for " + k)
				masterErrors["by_type"]["list_min_len"] = map[string]error{k: e}
				r.Err["list_min_len"] = e
				allErrors = append(allErrors, e)
			}
			// list max length check
			if (v.ListMaxLen != 0) && (len(r.Parsed) > v.ListMaxLen) {
				//r.Err = append(r.Err, errors.New("list size should not be more than "+strconv.Itoa(v.ListMaxLen)+" for "+k))
				e := errors.New("list size should not be more than " + strconv.Itoa(v.ListMaxLen) + " for " + k)
				r.Err["list_max_len"] = e
				masterErrors["by_type"]["list_max_len"] = map[string]error{k: e}
				allErrors = append(allErrors, e)

			}
			// list length check
			if (v.ListLength != 0) && (len(r.Parsed) != v.ListLength) {
				//r.Err = append(r.Err, errors.New("list size must be equal to "+strconv.Itoa(v.ListLength)+" for "+k))
				e := errors.New("list size must be equal to " + strconv.Itoa(v.ListLength) + " for " + k)
				r.Err["list_length"] = e
				masterErrors["by_type"]["list_length"] = map[string]error{k: e}
				allErrors = append(allErrors, e)

			}

			// enum check
			if v.Enum {
				errs := checkEnumList(k, v.EnumList, r.Raw, v.DataType, v.Seperator, v.CaseSensitive)
				if len(errs) > 0 {
					e := errors.New(strings.Join(errs, ","))
					r.Err["value_set"] = e
					masterErrors["by_type"]["value_set"] = map[string]error{k: e}
					allErrors = append(allErrors, e)

				}
			}

			// conditional field validations
			/*if v.Conditional {
				if len(v.RequiredFor) > 0 {
					err := checkRequiredFor(req, k, v.RequiredFor)
					if err != nil {
						r.Err = append(r.Err, err)
					}
				}
				if len(v.RequiredWith) > 0 {
					err := checkRequiredWith(req, k, v.RequiredFor)
					if err != nil {
						r.Err = append(r.Err, err)
					}
				}
				if len(v.RequiredAnyOne) > 0 {
					err := checkRequiredAnyOne()
					if err != nil {
						r.Err = append(r.Err, err)
					}
				}

			}*/

		}

		op[k] = r
		if len(r.Err) > 0 {
			masterErrors["by_field"][k] = r.Err
		}

	}

	//fmt.Println("errors by field")
	/*fmt.Println(masterErrors["by_field"])
	fmt.Println("errors by_type ")
	fmt.Println(masterErrors["by_type"])*/

	result := Result{
		op,
		masterErrors["by_type"],
		masterErrors["by_field"],
		allErrors,
	}

	//return op, masterErrors

	return result
}
func checkMinLength(s, dt, sep, fieldName string, fl int64) error {
	arrStr := make([]string, 0)
	if isListType(dt) {
		arrStr = strings.Split(strings.Trim(s, sep), sep)
	} else {
		arrStr = append(arrStr, s)
	}

	values := make([]string, 0)
	l := int(fl)
	var err error
	for _, v := range arrStr {
		if len(v) < l {
			values = append(values, v)
		}
	}
	if len(values) > 0 {
		err = errors.New("length of values (" + strings.Join(values, ",") + ") for " + fieldName + " should not be less than " + strconv.Itoa(l))
	}
	return err
}

func checkMaxLength(s, dt, sep, fieldName string, fl int64) error {
	l := int(fl)
	arrStr := make([]string, 0)
	if isListType(dt) {

		arrStr = strings.Split(strings.Trim(s, sep), sep)

	} else {
		arrStr = append(arrStr, s)
	}
	values := make([]string, 0)
	var err error
	for _, v := range arrStr {
		if len(v) > l {
			values = append(values, v)
		}
	}
	if len(values) > 0 {
		err = errors.New("length of values (" + strings.Join(values, ",") + ") for " + fieldName + " should not be more than " + strconv.Itoa(l))

	}
	return err
}
func checkLength(s, dt, sep, fieldName string, fl int64) error {
	l := int(fl)
	arrStr := make([]string, 0)
	if isListType(dt) {
		arrStr = strings.Split(strings.Trim(s, sep), sep)

	} else {
		arrStr = append(arrStr, s)
	}
	values := make([]string, 0)
	var err error
	for _, v := range arrStr {
		if len(v) != l {
			values = append(values, v)
		}
	}
	if len(values) > 0 {
		err = errors.New("length of values (" + strings.Join(values, ",") + ") for " + fieldName + " must be equal to " + strconv.Itoa(l))

	}
	return err
}
func fromValue(req *http.Request, key string) (bool, string) {
	if req.Form == nil {
		req.ParseForm()
	}

	val, ok := make([]string, 0), false
	if req.Method == "GET" {
		val, ok = req.Form[key]

	} else {
		val, ok = req.PostForm[key]
		/*if val, ok := req.PostForm[key]; ok {
			return ok, strings.Trim(val[0], " ")
		}*/
	}

	if ok {
		return ok, strings.Trim(val[0], " ")
	}
	return false, ""
}

func isListType(s string) bool {
	if len(s) > 3 {
		if (strings.ToLower(s[len(s)-4:]) == "list") || (strings.ToLower(s[len(s)-3:]) == "set") {
			return true
		}
		return false
	}
	return false
}

func checkEnumList(field string, enumList []string, rawVal, dataType, sep string, caseSensitive bool) []string {
	errs := make([]string, 0)
	if len(enumList) == 0 {
		errs = append(errs, "value set is not defined for "+field)
		return errs
	}
	if rawVal == "" {
		errs = append(errs, "value for "+field+" must be one of ["+strings.Join(enumList, ",")+"]")
		return errs
	}
	valList := make([]string, 0)
	if !caseSensitive {
		rawVal = strings.ToLower(rawVal)
	}
	if isListType(dataType) {
		valList = strings.Split(strings.Trim(rawVal, sep), sep)
	} else {
		valList = append(valList, rawVal)
	}

	for _, valIp := range valList {
		found := false

		for _, eVal := range enumList {
			eval := eVal
			if !caseSensitive {
				eval = strings.ToLower(eVal)
			}
			if eval == valIp {
				found = true
			}

		}
		if !found {
			errs = append(errs, valIp+" not found in value list ["+strings.Join(enumList, ",")+"] for field "+field)
		}

	}

	return errs
}

func checkRequiredAnyOne(req *http.Request, fieldList []string) (error, string) {
	// sortign fields to get same error every time
	sort.Strings(fieldList)
	err_key := strings.Join(fieldList, ",")
	for _, v := range fieldList {
		_, fieldValue := fromValue(req, v)
		if fieldValue != "" {
			return nil, err_key
		}
	}
	return errors.New("at least one of [" + err_key + "] must not be empty"), err_key
}

func checkRequiredEitherOne(req *http.Request, fieldList []string) (error, string) {
	sort.Strings(fieldList)
	err_key := strings.Join(fieldList, ",")
	count := 0
	for _, v := range fieldList {
		_, fieldValue := fromValue(req, v)
		if fieldValue != "" {
			count += 1
		}
	}
	if count == 0 {
		return errors.New("one of among [" + err_key + "] must have value"), err_key
	}
	if count == 1 {
		return nil, err_key
	}
	return errors.New("only one of among [" + err_key + "] should have value"), err_key

}

func checkRequiredWith(field string, req *http.Request, requiredWith []string) error {
	_, fieldVal := fromValue(req, field)
	if fieldVal != "" {
		return nil
	}
	for _, v := range requiredWith {
		_, rqVal := fromValue(req, v)
		if rqVal != "" && fieldVal == "" {
			return errors.New("parameter " + field + " must not be empty when parameter " + v + " has value")
		}
	}
	return nil

}

func checkRequiredFor(field string, req *http.Request, caseSessitive bool, reqFor []struct {
	Field    string   `json:"field"`
	Value    []string `json:"value"`
	AnyValue bool     `json:"any_value"`
}) error {
	_, fieldVal := fromValue(req, field)
	if fieldVal != "" {
		return nil
	}

	for _, v := range reqFor {
		_, reqForVal := fromValue(req, v.Field)
		if reqForVal != "" && fieldVal == "" {
			if v.AnyValue {
				return errors.New("parameter " + field + " must not be empty when parameter" + v.Field + " has value")
			} else {
				if inArray(reqForVal, v.Value, caseSessitive) {
					return errors.New("parameter " + field + " must not be empty when parameter " + v.Field + " is " + reqForVal)
				}
			}
		}

	}
	return nil
}

func inArray(reqForVal string, list []string, caseSensitive bool) bool {
	if !caseSensitive {
		reqForVal = strings.ToLower(reqForVal)
	}

	for _, v := range list {
		if !caseSensitive {
			v = strings.ToLower(v)
		}

		if v == reqForVal {
			return true
		}
	}
	return false
}

func isPastDate(dates []interface{}) (bool, int64) {
	date := dates[0].(time.Time)

	duration := date.Sub(time.Now())
	hours := duration.Hours()
	absoluteDiff := int64(math.Floor(math.Abs(hours) / 24))

	if hours > 0 {
		// this is future date
		return false, absoluteDiff

	}
	return true, absoluteDiff
}

type errStructure struct {
	Code      string `json:"code"`
	Desc      string `json:"desc"`
	Title     string `json:"title"`
	ErrorType string `json:"type"`
}

type ErrMap []errStructure

//func (r Result) FormatError(t map[string]map[string]string) map[string][]errStructure {
func (r Result) FormatError(t map[string]map[string]string) []errStructure {
	/*fmt.Println("format result validator2")
	fmt.Printf("%+v\n", t)*/
	opErr := make([]errStructure, 0)
	if _, ok := t["default"]; !ok {
		t["default"] = map[string]string{
			"type":  "Undocumented",
			"title": "UnDocumented",
			"doce":  "X0001",
			"desc":  "some validation error happend for field {{ .fieldName }}",
		}
	}

	//errOp := make(map[string]map[string]map[string]string)
	for f, v := range r.Fields {
		if len(v.Err) > 0 {
			//opTtpl := make(map[string]map[string]string)
			//opArrTpl := make([]errStructure, 0)

			for et := range v.Err {
				if _, ok := t[et]; !ok {

					fmt.Println("no error template defined for :", et)
					et = "default"
				}
				eStr := errStructure{
					Desc:      parseTemplate(t[et]["desc"], getValueList(f, v)),
					ErrorType: t[et]["type"],
					Title:     t[et]["title"],
					Code:      t[et]["code"],
				}
				opErr = append(opErr, eStr)
				//opArrTpl = append(opArrTpl, eStr)

			}
			//errOp[f] = opTtpl
			//opErr[f] = opArrTpl
		}
	}

	//fmt.Printf("%+v", opErr)
	return opErr

}

func parseTemplate(s string, v map[string]interface{}) string {
	t := template.New(v["fieldName"].(string))
	b := bytes.Buffer{}
	t.Parse(s)
	if err := t.Execute(&b, v); err != nil {
		fmt.Println(err)
	}
	return b.String()

}

func getValueList(f string, v Output) map[string]interface{} {
	op := make(map[string]interface{})
	op["fieldName"] = f
	op["value"] = v.Raw
	op["dataType"] = v.DataType
	op["valueSet"] = strings.Join(v.Conf.EnumList, ",")
	op["requiredWith"] = strings.Join(v.Conf.RequiredWith, ",")
	op["format"] = v.Conf.DateFormat
	op["length"] = v.Conf.Length
	op["maxLength"] = v.Conf.MaxLen
	op["minLength"] = v.Conf.MinLen

	return op
}

func NewResult() Result {
	return Result{}
}
