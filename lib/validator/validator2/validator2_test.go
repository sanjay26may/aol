package validator2

import (
	//"aol/lib/validator/validaror2"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"testing"
)

type ReqFor struct {
	Field    string        `json:"field"`
	Value    []interface{} `json:"value"`
	AnyValue bool          `json:"any_value"`
}
type FConfig struct {
	DataType     string   `json:"data_type"`
	Required     bool     `json:"required"`
	Enum         bool     `json:"enum"`
	EnumList     []string `json:"enum_list"`
	Conditional  bool     `json:"conditional"`
	RequiredFor  []ReqFor `json:"required_for"`
	Length       int64    `json:"length"`
	RequiredWith []string
}

//var Fields map[string]FConfig

/*var Fields map[string]struct {
	DataType    string   `json:"data_type"`
	Required    bool     `json:"required"`
	Enum        bool     `json:"enum"`
	EnumList    []string `json:"value_set"`
	Conditional bool     `json:"conditional"`
	RequiredFor []struct {
		Field    string   `json:"field"`
		Value    []string `json:"value"`
		AnyValue bool     `json:"any_value"`
	} `json:"required_for"`
	Length          int64    `json:"length"`
	MinLen          int64    `json:"min_len"`
	MaxLen          int64    `json:"max_len"`
	RequiredWith    []string `json:"required_with"`
	RequiredAnyOne  []string `json:"required_any_one"`
	DateFormat      string   `json:"format"`
	Seperator       string   `json:"seperator"`
	CheckDisposable bool     `json:"check_disposable"`
	ListMinLen      int      `json:"list_min_len"`
	ListMaxLen      int      `json:"list_max_len"`
	ListLength      int      `json:"list_length"`
	CaseSensitive   bool     `json:"case_sensitive"`
}
*/
func TestOutput(t *testing.T) {

	d := `{
  "type":{"data_type":"id_string_set",
  			"required":true,
  			"enum":true,
  			"value_set":["search","country"],
  			"case_sensitive":false
  			
  			},
  "country":{"data_type":"iso2country",
            "conditional":true,
            "required_for":[
                        {"field":"type","value":["Country"],"any_value":false},
                        {"field":"mctype","value":[],"any_value":true}
                      ],
            "length":2,
            "case_sensitive":false
              },
  "lat":{"data_type":"float",
          "conditional":true,
          "required_for":[
                          {"field":"type","value":["search"],"any_value":false}
                        ],
          "required_with":["lon"]
        },
  "lon":{"data_type":"float",
          "conditional":true,
          "required_for":[
              {"field":"type","value":["search"],"any_value":false}
          ],
          "required_with":["lat"]
        },
    "email":{
        "data_type":"email_set",
        "max_len":17,
        "check_disposable":true,
        "seperator":"|",
        "list_min_len":1,
        "list_max_len":3    
    },
    "name":{"data_type":"string",
    		"conditional":true,
    		"required_any_one":["email","name"]

    },
    "start_date":{
    	"data_type":"date",
    	"format":"YYYY-MM-DD",
    	"future_date":true,
    	"day_diff":5
    }

}`

	errTemplate := `{
	"required":{
		"type":"Business error",
		"title":"MissingRequiredField",
		"desc":"Value for {{ .fieldName }} is missing"
	},
	"data_type-date":{
		"type":"Data error",
		"title":"InvalidDate",
		"desc":"Format provided for {{ .fieldName }} is invalid or invalid date {{.value }} found. Expected format is {{ .format }}"
	},
	"required_with":{
		"type":"Business error",
		"title":"MissingConditionalField",
		"desc":"field {{ .fieldName }} is required with {{ .requiredWith}} "
	},
	"value_set":{
		"type":"Data error",
		"title":"InvalidValue",
		"desc":"field {{ .fieldName }} can have [{{ .valueSet }}] values. Value provided is {{ .value}}"
	},
	"length":{
		"type":"Business error",
		"title":"InvalidFieldSize",
		"desc":"length of value provided for {{ .fieldName }} exceeds the allowed limit of {{ .length }} "
	},
	"data_type-number":{
		"type":"Data error",
		"title":"InvalidDataType",
		"desc":"allowed data type for {{ .fieldName}} is {{ .dataType }}. Provided value is {{ .value }} "
	}
}`

	/*
		date: not in future
		not in past


	*/

	var fields ConfigStructure
	err := json.Unmarshal([]byte(d), &fields)
	if err != nil {
		fmt.Println(err)
	}

	et := make(map[string]map[string]string)

	err = json.Unmarshal([]byte(errTemplate), &et)
	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println(Fields)
	buf := bytes.NewBufferString("name=sanjay")
	//req := httptest.NewRequest("GET", "http://www.example.com?type=country&email=saome2@gmail.com|saome@gmail.com|asdf@yopmail.com|saome@gmail.com&lon=77.234&start_date=2017-11-23", nil)
	req := httptest.NewRequest("POST",
		"http://www.example.com?ancd=some&type=SearcH,country&lat=77.234&name=sanj",
		nil)
	req.Write(buf)
	result := Validate(req, fields)
	/*for k, v := range result.Fields {
		&fmt.Println("field_name", k)
		fmt.Println("data type", v.DataType)
		fmt.Println("raw value", v.Raw)
		fmt.Println("parsed value", v.Parsed)
		fmt.Println("error", v.Err)
		fmt.Println("param present", v.ParamPresent)
		fmt.Println("************************************************")

		//fmt.Println(v.Missing)
	}*/

	//fmt.Println(errorList)
	if len(result.AllErrors) > 0 {
		fmt.Println("validation errors")

		pe := result.FormatError(et)
		fmt.Printf("%+v\n", pe)
		fmt.Println("________________")
		for _, v := range result.AllErrors {
			fmt.Println(v)
		}
	} else {
		fmt.Println("validation success")
	}
	//t.Log(op)

}
